import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

describe('AppComponent', () => {
	let app: AppComponent;
	let fixture: ComponentFixture<AppComponent>;
	let compiled: HTMLElement;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [
				AppComponent
			],
			imports: [
				BrowserModule,
				AppRoutingModule,
			]
		}).compileComponents();

		fixture = TestBed.createComponent(AppComponent);
		app = fixture.componentInstance;
		fixture.detectChanges();
		compiled = fixture.nativeElement as HTMLElement;
	});

	it('should create the app', () => {
		expect(app).toBeTruthy();
	});

	it(`should have as title 'boilerplate_ng18'`, () => {
		expect(app.title).toEqual('boilerplate_ng18');
	});

	it('should render title', () => {
		expect(compiled.querySelector('h1')?.textContent).toContain('Hello, boilerplate_ng18');
	});
});
