# BoilerplateNg18

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 18.0.3.

## Configuration

The project's name must be set in `package.json`, `package.lock.json` and in `src/app/app.component`.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `npm run test` to execute the unit tests via jest.

Run `npm run test:watch` to execute the unit tests at each save.

Run `npm run test:coverage` to get the unit tests coverage rate.

Run `npm run test:cow` to execute the unit tests at each save and get the coverage rate.

## Running esLint

Run `npm run lint` to parse the entire code.

Run `npm run lint:fix`to parse the entire code and fix errors.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.dev/tools/cli) page.
