// @ts-check
const eslint = require("@eslint/js");
const tseslint = require("typescript-eslint");
const angular = require("angular-eslint");
  
module.exports = tseslint.config(
  {
    files: ["**/*.ts"],
    extends: [
      eslint.configs.recommended,
      ...tseslint.configs.recommended,
      ...tseslint.configs.stylistic,
      ...angular.configs.tsRecommended,
    ],
    processor: angular.processInlineTemplates,
    rules: {
      "@angular-eslint/directive-selector": [
        "error",
        {
          type: "attribute",
          prefix: "app",
          style: "camelCase",
        },
      ],
      "@angular-eslint/component-selector": [
        "error",
        {
          type: "element",
          prefix: "app",
          style: "kebab-case",
        },
      ],
      "semi": "error",
      "quotes": [
        "error",
        "single",
        {
          "avoidEscape": true,
          "allowTemplateLiterals": true,
        }
      ],
      "indent": [
        "error",
        "tab"
      ],
      "function-paren-newline": ["error", { "minItems": 2 }],
      "function-call-argument-newline": ["error", "always"],
      /* "camelcase": [
        "error",
        {
          "properties": "always",
          "ignoreImports": true,
        }
      ], */
      "arrow-spacing": ["error", { "before": true, "after": true }],
      "brace-style": ["error", "1tbs"],
      "block-spacing": ["error", "always"],
      "function-call-spacing": ["error", "never"],
      "key-spacing": ["error", { "beforeColon": false, "afterColon": true, "mode": "strict" }],
      "keyword-spacing": ["error", { "before": true, "after": true }],
      "multiline-ternary": ["error", "always"]
    },
  },
  {
    files: ["**/*.html"],
    extends: [
      ...angular.configs.templateRecommended,
      ...angular.configs.templateAccessibility,
    ],
    rules: {},
  }
);
